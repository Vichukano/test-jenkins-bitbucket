package ru.vichukano.test;

public class TestJenkins {
    private String name;
    private String surname;

    public TestJenkins() {

    }

    public TestJenkins(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public String hello() {
        return String.format("Hello, %s %s!", this.name, this.surname);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}
