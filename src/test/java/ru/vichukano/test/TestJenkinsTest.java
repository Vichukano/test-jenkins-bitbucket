package ru.vichukano.test;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestJenkinsTest {

    @Test
    public void whenCallHelloWithArgumentsThenReturnString() {
        TestJenkins test = new TestJenkins("test", "test");
        assertEquals(test.hello(), "Hello, test test!");
    }

    @Test
    public void whenCallHelloWithoutArgumentsAndSetItThenReturnString() {
        TestJenkins test = new TestJenkins();
        test.setName("test");
        test.setSurname("test");
        assertEquals(test.hello(), "Hello, test test!");
    }

    @Test
    public void whenCallHelloWithoutArgumentsThenReturnStringHello() {
        TestJenkins test = new TestJenkins();
        assertEquals(test.hello(), "Hello, null null!");
    }
}
